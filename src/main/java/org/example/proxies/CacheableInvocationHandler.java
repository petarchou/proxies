package org.example.proxies;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheableInvocationHandler implements InvocationHandler {

    private final Map<String, Object> cache = new ConcurrentHashMap<>();
    private final Object realObject;

    public CacheableInvocationHandler(Object realObject) {
        this.realObject = realObject;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Cacheable cacheable = realObject.getClass()
                .getMethod(method.getName(), method.getParameterTypes())
                .getAnnotation(Cacheable.class);

        if (cacheable == null) {
            return method.invoke(realObject, args);
        } else {
            String cacheId = cacheable.value();
            Object cachedValue = cache.get(cacheId);
            if(cachedValue == null) {
                cachedValue = method.invoke(realObject,args);
                cache.put(cacheId,cachedValue);
            }

            return cachedValue;
        }
    }
}
