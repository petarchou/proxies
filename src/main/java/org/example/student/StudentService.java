package org.example.student;

import java.util.List;

public interface StudentService {

    List<Student> getAllStudents();

}
