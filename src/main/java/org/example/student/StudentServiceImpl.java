package org.example.student;

import org.example.proxies.Cacheable;

import java.util.List;

public class StudentServiceImpl implements StudentService{



    @Cacheable("students")
    @Override
    public List<Student> getAllStudents() {
        System.out.println("Calculating students...");

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return List.of(
                new Student(1L,"Pesho", 23),
                new Student(2L,"Todor", 32)
        );
    }
}
