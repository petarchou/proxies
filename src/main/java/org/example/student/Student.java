package org.example.student;

public record Student(Long id, String name, Integer age) {
}
