package org.example;

import org.example.proxies.CacheableInvocationHandler;
import org.example.student.StudentService;
import org.example.student.StudentServiceImpl;

import java.lang.reflect.Proxy;

public class StudentController {
    public static void main(String[] args) {

        StudentService service = pretendThisIsSpringAndInjectsObject();

        System.out.println(service.getAllStudents());
        System.out.println("Waiting 2 seconds...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(service.getAllStudents());

    }

    private static StudentService pretendThisIsSpringAndInjectsObject() {

        StudentServiceImpl realObject = new StudentServiceImpl();

       return (StudentService) Proxy.newProxyInstance(StudentController.class.getClassLoader(),
                new Class[]{StudentService.class},
                new CacheableInvocationHandler(realObject));
    }
}
